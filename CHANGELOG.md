# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.1] - 2021-02-25
### Fix
- Wrong Caraya destination in standard version
- Remove latest from build_latest.ps1
- Removed recommended packages install from ECM version of LabVIEW 

## [0.5.0] - 2021-01-25
### Added
- Build script for whole solution
- Rename folders to adapt solution

### Changed
- Updated Readme

## [0.4.0] - 2021-01-25
### Added
- Caraya 1.0.2 beta
- ECM Versions for LabVIEW, Caraya and VI Analyzer.
- SDM Version updated to 0.2.6

## [0.3.3] - 2020-12-14
### Changed
- Update VI Analyzer Custom CLI Operation

## [0.3.2] - 2020-12-10
### Changed
- Update LabVIEW 20.0f1 to NIPM 20.6
- Update VI Analyzer to NIPM 20.6 and include VI Analyzer Custom CLI Operation

## [0.3.1] - 2020-12-10
### Changed
- Included the NI Package Builder into NIPM 20.6 Docker Image

## [0.3.0] - 2020-12-02
### Added
- NIPM 20.6 Dockerfile
- SDM 0.2.2 Dockerfile
- Caraya 1.1.0 Dockerfile

## [0.3.0] - 2020-11-03
### Added
- SDM 0.1.0 Dockerfile

## [0.2.0] - 2020-09-16
### Added
- LV 2019 SP1 Dockerfile
- LV 2020 x64 Dockerfile

## [0.1.1] - 2020-09-11
### Fix
- Change in NIPM Docker file to use Windows 1909 as base image

## [0.1.0] - 2020-09-09
### Added
- Initial Version
