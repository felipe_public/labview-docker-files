# LabVIEW Docker Files
This repository contains docker files for building LabVIEW componentes into docker images.\
Some of these images are publicly availabe in [Felipe Docker Hub profile](https://hub.docker.com/u/felipefoz).

## Author
- Felipe Pinheiro Silva

### Contributors
- Marcos Alves Balsamo
- Patric Jenni

## Requirements
- Windows Pro <= build 1909 (Gitlab Runner Requirement).
- Docker for Windows and Switch Windows Containers.
- Windows Containers and Hyper-V features enabled.

## Usage
After installing, you may build your own images from docker commands, substituting the appropriate fields.

```code
docker build --rm -f "ChosenDockerFile" -t NameOfTheImage:TagOfTheImage "."
```

### Scripting
There is an automated scripting for generating images in sequence, respecting the layers.\
This way, it is easy to modify the base image and reflect the changes in the others.\
The latest tag is also included.

```mermaid
graph LR
NIPM --> LabVIEW & SDM
LabVIEW --> VIAnalyzer & Caraya
LabVIEW --> LabVIEW-ECM
LabVIEW-ECM --> VIAnalyzer-ECM & Caraya-ECM
```


## Contributing
Create issues for bugs or suggestions.\
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.\
Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
