#If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
 #   [Security.Principal.WindowsBuiltInRole] �Administrator�))

#{
 #   Write-Warning �You do not have Administrator rights to run this script!`nPlease re-run this script as an Administrator!�
  #  Break
#}

$docker_user="felipefoz"

$src_folder="src"

$nipm_version="20.6"

$sdm_version="0.2.6"

$labview_version="20.0f1"

$vianalyzer_version="20.0"

$caraya_version="1.2.0b-20.0"

$labview_ecm_version="20.0f1"

$vianalyzer_ecm_version="20.0f1-ecm"

$caraya_ecm_version="1.2.0b-20.0-ecm"


#ToDo build a for loop for packages
#ToDo use docker envs to shift versions inside dockerfile

# NIPM
echo "Building NPM Image version $nipm_version..."
$package_name="nipm"
$package_version=$nipm_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version -t $docker_user/${package_name}:latest ".\$src_folder\$package_name\$package_version"

echo "Pushing NIPM Image..."
docker image push --all-tags $docker_user/nipm

#SDM
echo "Building SDM Image version $sdm_version..."
$package_name="sdm"
$package_version=$sdm_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version -t $docker_user/${package_name}:latest ".\$src_folder\$package_name\$package_version"

echo "Pushing SDM Image..."
docker image push --all-tags $docker_user/sdm

#LabVIEW
echo "Building LabVIEW Image version $labview_version..."
$package_name="labview"
$package_version=$labview_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version -t $docker_user/${package_name}:latest ".\$src_folder\$package_name\$package_version"

echo "Pushing LabVIEW Image..."
docker image push --all-tags $docker_user/labview

#VI Analyzer
echo "Building VI Analyzer Image version $vianalyzer_version..."
$package_name="vianalyzer"
$package_version=$vianalyzer_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version -t $docker_user/${package_name}:latest ".\$src_folder\$package_name\$package_version"

echo "Pushing VI Analyzer Image..."
docker image push --all-tags $docker_user/vianalyzer

#Caraya
echo "Building Caraya Image version $caraya_version..."
$package_name="caraya"
$package_version=$caraya_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version -t $docker_user/${package_name}:latest ".\$src_folder\$package_name\$package_version"

echo "Pushing Caraya Image..."
docker image push --all-tags $docker_user/caraya

#LabVIEW-ECM
echo "Building LabVIEW Embedded Control and Monitoring Image version $labview_ecm_version..."
$package_name="labview-ecm"
$package_version=$labview_ecm_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version -t $docker_user/${package_name}:latest ".\$src_folder\$package_name\$package_version"

echo "Pushing LabVIEW ECM Image..."
docker image push --all-tags $docker_user/labview-ecm

#VI Analyzer ECM
echo "Building VI Analyzer ECM Image version $vianalyzer_ecm_version..."
$package_name="vianalyzer"
$package_version=$vianalyzer_ecm_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version  ".\$src_folder\$package_name\$package_version"

echo "Pushing VI Analyzer ECM Image..."
docker image push --all-tags $docker_user/vianalyzer

#Caraya ECM
echo "Building Caraya ECM Image version $vianalyzer_ecm_version..."
$package_name="caraya"
$package_version=$caraya_ecm_version
docker build --rm -f .\$src_folder\$package_name\$package_version\Dockerfile -t $docker_user/${package_name}:$package_version ".\$src_folder\$package_name\$package_version"

echo "Pushing Caraya ECM Image..."
docker image push --all-tags $docker_user/caraya