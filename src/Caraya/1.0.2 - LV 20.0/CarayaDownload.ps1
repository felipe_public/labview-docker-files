﻿$baseUrl = 'http://www.jkisoft.com/packages/packages/'

$files = @('jki_lib_caraya/jki_lib_caraya-1.0.2.115', 'oglib_appcontrol/oglib_appcontrol-4.1.0.7',`
'jki_lib_state_machine/jki_lib_state_machine-2018.0.7.45', 'oglib_file/oglib_file-4.0.1.22', 'oglib_string/oglib_string-4.1.0.12', 'oglib_lvdata/oglib_lvdata-4.2.0.21', `
'oglib_array/oglib_array-4.1.1.14', 'oglib_error/oglib_error-4.2.0.23')

$j=0
foreach ($file in $files)
{
   Write-Host "Downloading $file..."
   $dlUrl = "$($baseUrl)$file.vip"
   $dlPath = "$j.zip"
   Invoke-WebRequest $dlUrl -OutFile $dlPath
   Write-Host "Uncompressing $file..."
   Expand-Archive -Path "$j.zip" -DestinationPath . -Force
   Remove-Item -Path "$j.zip"
   
   $j++
}

#LabVIEWCLI.exe -OperationName MassCompile −DirectoryToCompile "File Group 0\"
#Write-Host "Copying to final destination..."
#Copy-Item -Path "File Group 0\*" -Destination 'c:\Program Files (x86)\National Instruments\LabVIEW 2020\' -Force -Recurse
#Write-Host "Cleaning..."
#Remove-Item -Path * -Force -Recurse