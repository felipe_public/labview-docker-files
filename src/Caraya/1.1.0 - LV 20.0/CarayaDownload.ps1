﻿$baseUrl = 'http://www.jkisoft.com/packages/packages/'

$files = @('jki_lib_caraya/jki_lib_caraya-1.1.0.119')
$j=0

foreach ($file in $files)
{
   Write-Host "Downloading $file..."
   $dlUrl = "$($baseUrl)$file.vip"
   $dlPath = "$j.zip"
   Invoke-WebRequest $dlUrl -OutFile $dlPath
   Write-Host "Uncompressing $file..."
   Expand-Archive -Path "$j.zip" -DestinationPath . -Force
   Remove-Item -Path "$j.zip"
   
   $j++
}

NILicensingCmd.exe /silent /addservers NI-VLM-TESTE

LabVIEWCLI.exe -OperationName MassCompile −DirectoryToCompile "File Group 0\"
Write-Host "Copying to final destination..."
Copy-Item -Path "File Group 0\*" -Destination 'c:\Program Files (x86)\National Instruments\LabVIEW 2020\' -Force -Recurse
Write-Host "Cleaning..."
Remove-Item -Path * -Force -Recurse